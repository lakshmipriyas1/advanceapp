import { Component, OnInit } from '@angular/core';
import { ModalController} from '@ionic/angular';
import { LoginPage } from '../login/login.page';
import { NgForm, FormBuilder ,Validators,FormControl, FormGroup} from '@angular/forms';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})

export class RegisterPage implements OnInit {
 todo = {
    fName: '',
    lName: '',
    username: '',
    password: ''
  };
  // validations_form:any;
  // matching_password_group:FormGroup;
  constructor(
               private authService: AuthService,
               private router: Router,
               private formBuilder:FormBuilder
    ) { 
      // this.validations_form = this.formBuilder.group({
      //   username: new FormControl('', Validators.required),
      //   firstname: new FormControl('', Validators.required),
      //   lastname: new FormControl('', Validators.required),
      //   password: new FormControl('', Validators.compose([
      //     Validators.minLength(5),
      //     Validators.required,
      //     Validators.pattern('^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])[a-zA-Z0-9]+$')
      //   ])),
      //   confirm_password: new FormControl('', Validators.required)
      // }, (formGroup: FormGroup) => {
      //   if(formGroup.value.password===formGroup.value.confirm_password){
      //     return true;
      //   }else{
      //     return{
      //       areEqual:false
      //     }
      //   }
      // });
        
      
    }
    // validation_messages = {
    //   'username': [
    //   // { type: 'pattern', message: 'Username must contain only combination of letterns and numbers.' },
    //   { type: 'required', message: 'Username is required.' }
    //   ],
    //   'firstname': [
    //   // { type: 'pattern', message: 'Username must contain atleast one letterns and one numbers.' },
    //   { type: 'required', message: 'Username is required.' }
    //   ],
    //   'lastname': [
    //   // { type: 'pattern', message: 'Username must contain only combination of letterns and numbers.' },
    //   { type: 'required', message: 'Username is required.' }
    //   ],
    //   'password': [
    //   { type: 'minlength', message: 'Password must be minimum 5 characters.' },
    //   { type: 'required', message: 'Password is required.' },
    //   { type: 'pattern', message: 'Password must contain combination of upper and lower case letterns and numbers.' }
    //   ],
    //   'confirm_password': [
    //   { type: 'required', message: 'Password is required.' },
    //   { type: 'areEqual', message: 'Confirm password is not same.' },
    //   ],

    // }

  ngOnInit() {
    console.log("Register");
  }
  register(form: NgForm) {
    this.authService.register(form.value.fName, form.value.lName, form.value.username, form.value.password).subscribe(
      data => {
        this.authService.login(form.value.username, form.value.password).subscribe(
          () => {
          },
          error => {
            console.log(error);
          },
          () => {
            this.router.navigateByUrl('/dashboard');
          }
        );
        },
        error => {
          console.log(error);
        },
        () => {
        }
    );
      }
}
