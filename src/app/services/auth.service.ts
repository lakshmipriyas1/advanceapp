import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { NativeStorage } from '@ionic-native/native-storage/ngx/index';
import { tap } from 'rxjs/operators';
import { User } from '../models/users';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  API_URL = 'http://192.168.5.59:4200/api/users';
  isLoggedIn: boolean;
  token: any;
  constructor( private http: HttpClient,
               private storage: NativeStorage,
               private router: Router,
               ) { }
               login(username: string, password: string) {
                return this.http.post(this.API_URL + '/login', {username, password}
                ).pipe(
                  tap(token => {
                    const saveToken: any = token;
                    localStorage.setItem('token', saveToken);
                    this.token = token;
                    this.isLoggedIn = true;
                    return token;
                  }),
                );
              }
  register(firstName: string, lastName: string, username: string, password: string) {
    return this.http.post(this.API_URL + '/register', {firstName, lastName, username, password});
    console.log(this.API_URL);
  }
  logout() {
  localStorage.clear();
  this.router.navigateByUrl('/landing');
  }

  user() {
    const userToken = localStorage.getItem('token');
    const headers = new HttpHeaders({Authorization: `Bearer ${userToken}`});

    console.log(headers);

    return this.http.get<User>(this.API_URL + '/current', { headers })
    .pipe(
      tap(user => {
        console.log(user);
        return user;
      })
    );
  }
  getToken() {
    return this.storage.getItem('token').then(
      data => {
        this.token = data;
        if (this.token != null) {
          this.isLoggedIn = true;
        } else {
          this.isLoggedIn = false;
        }
      },
      error => {
        this.token = null;
        this.isLoggedIn = false;
      }
    );
  }
  // popupData(){
  //   const userToken = localStorage.getItem('token');
  //   const headers = new HttpHeaders({Authorization: `Bearer ${userToken}`});
  //   return this.http.get<User>(this.API_URL + '/current', { headers })
  //   .pipe(
  //     tap(user => {
  //       console.log(user);
  //       return user;
  //     })
  //   );
  // }
}
