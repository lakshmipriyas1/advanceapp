import { Component, OnInit } from '@angular/core';
import { HttpHeaders , HttpClient} from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
import { Injectable } from '@angular/core';
import { NavextraserviceService } from '../services/navextraservice.service';
import { NgForm } from '@angular/forms';
import { from } from 'rxjs';

@Component({
  selector: 'app-viewpost',
  templateUrl: './viewpost.page.html',
  styleUrls: ['./viewpost.page.scss'],
})

@Injectable({
  providedIn: 'root'
})
export class ViewpostPage implements OnInit {
details: any;
// commen:'';
  constructor(private http: HttpClient, private router: Router,
              private activatedRoute: ActivatedRoute,
              private navextraservice: NavextraserviceService) {
                  this.details = navextraservice.getExtras();
                  console.log(this.details);
               }

  ngOnInit() {
  }
  comment(form:NgForm)
  {
    // console.log(form.value.comm);
    // console.log(event);
    const Token = localStorage.getItem('token');
    const headers = new HttpHeaders({Authorization: `Bearer ${Token}`});
    return this.http.post('http://192.168.5.59:4200/api/users/posts/' +this.details._id+'/addComment',{text:form.value.comm}, { headers })
    .subscribe((data: any) => {
      alert(data.message);

  }, error => {
    console.log(error.error.message);

  });
  }
  // ionViewDidLoad(){}
}
