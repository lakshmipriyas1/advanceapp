import { Component, OnInit } from '@angular/core';

import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-landing',
  templateUrl: './landing.page.html',
  styleUrls: ['./landing.page.scss'],
})
export class LandingPage implements OnInit {
  constructor(
    private authService: AuthService,
    private router: Router
  ) {
  }
  ionViewWillEnter() {
    this.authService.getToken().then(() => {
      if (this.authService.isLoggedIn) {
        this.router.navigateByUrl('/dashboard');
      }
    });
  }
  ngOnInit() {

  }
  // async register() {
  //   const registerModal = await this.modalController.create({
  //     component: RegisterPage
  //   });
  //   return await registerModal.present();
  // }
  // async login() {
  //   const loginModal = await this.modalController.create({
  //     component: LoginPage,
  //   });
  //   return await loginModal.present();
  // }
}
