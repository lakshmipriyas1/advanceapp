import { Component, OnInit, NgModule } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { ModalController } from '@ionic/angular';
import { User } from 'src/app/models/users';
import { CommonModule } from '@angular/common';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { Router } from '@angular/router';
import { NavextraserviceService } from '../services/navextraservice.service';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss'],
})
@NgModule({
  imports: [CommonModule]
})
export class DashboardPage implements OnInit {
  user: User;
  array = [];
  dropview:any;
  constructor( private authService: AuthService,
               private modalController: ModalController,
               private http: HttpClient,
               private router: Router,
               private navextraservice: NavextraserviceService) {
               }
  ngOnInit() {
    const Token = localStorage.getItem('token');
    const headers = new HttpHeaders({Authorization: `Bearer ${Token}`});
    return this.http.get('http://192.168.5.59:4200/api/users/posts' + '/viewPosts', { headers })
    .subscribe((data: any) => {
        this.array = data;
    }, error => {
      console.log(error.error.message);

    });
  }
  ionViewWillEnter() {
    this.authService.user().subscribe(
      user => {
        console.log(user);
        this.user = user;
      }
    );
  }
  dismissLogout() {
    this.modalController.dismiss();
  }
  logout() {
    this.authService.logout();
  }
  doRefresh(event) {
    console.log('Begin async operation');

    setTimeout(() => {
      console.log('Async operation has ended');
      event.target.complete();
    }, 1000);
    const Token = localStorage.getItem('token');
    const headers = new HttpHeaders({Authorization: `Bearer ${Token}`});
    return this.http.get('http://192.168.5.59:4200/api/users/posts' + '/viewPosts', { headers })
    .subscribe((data: any) => {
        this.array = data;
    }, error => {
      console.log(error.error.message);

    });
  }
  viewPost(postId:string) {
    // consopostId;
    console.log(postId);
    const Token = localStorage.getItem('token');
    const headers = new HttpHeaders({Authorization: `Bearer ${Token}`});
    //console.log(localStorage.getItem('token'));
    return this.http.get('http://192.168.5.59:4200/api/users/posts' + '/getPost/'+postId, { headers })
    .subscribe((data: any) => {
      this.navextraservice.setExtras(data);
      this.router.navigateByUrl('/viewpost');

  }, error => {
    console.log(error.error.message);

  });
  }

  myPosts()
  {
    console.log(this.dropview);
    if(this.dropview==="myposts")
    {
      const Token = localStorage.getItem('token');
      const headers = new HttpHeaders({Authorization: `Bearer ${Token}`});
      return this.http.get('http://192.168.5.59:4200/api/users/posts' + '/viewMyPosts', { headers })
      .subscribe((data: any) => {
          this.array = data;
      }, error => {
        console.log(error.error.message);

      });
    }else{
      const Token = localStorage.getItem('token');
      const headers = new HttpHeaders({Authorization: `Bearer ${Token}`});
      return this.http.get('http://192.168.5.59:4200/api/users/posts' + '/viewPosts', { headers })
      .subscribe((data: any) => {
          this.array = data;
      }, error => {
        console.log(error.error.message);

      });
    }

    
  }
}


