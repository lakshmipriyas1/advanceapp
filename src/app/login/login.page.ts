import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { NgForm } from '@angular/forms';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';
import { RegisterPage } from '../register/register.page';
// @ViewChild('password') password:ElementRef;
@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  todo = {
  username: '',
  password: ''
};
passwordshown:boolean;
  constructor(private authService: AuthService,
              private router: Router,
    ) {
      this.passwordshown=false;
    }

  ngOnInit() {
    //console.log("Register");
  }
 async login(form: NgForm) {
    this.authService.login(form.value.username, form.value.password).subscribe(
      data => {
      },
      error => {
        alert('Please Enter Valid Credentials');
        console.log(error);
      },
      () => {
        alert('Successfully Logined');
        this.router.navigateByUrl('/dashboard');
      }
    );
  }
  show(){
    // var pass=document.getElementById("mypassword");
  
this.passwordshown=!this.passwordshown;
  }
}
