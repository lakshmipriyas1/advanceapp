import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { User } from 'src/app/models/users';
import { HttpClient } from '@angular/common/http';


@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {
  user: User;
  constructor(
              private authservice: AuthService,
              private http: HttpClient) { }

  ngOnInit() {
  }
  ionViewWillEnter() {
    this.authservice.user().subscribe(
      user => {
        console.log(user);
        this.user = user;
      }
    );
  }
}
